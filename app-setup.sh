#!/bin/bash

if [ ! -e /tmp/bottle-app ]
then
    sudo mkdir -p /tmp/bottle-app
fi

sudo chmod -R 777 /tmp/bottle-app

sudo apt-get install python3 -y


cd /tmp
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

pip install bottle

ipaddr=`ip a | grep inet | grep 10.10 | awk '{print $2}' | cut -f 1 -d '/'`

cat >/tmp/bottle-app/app.py << EOF
from bottle import route, run
import platform

@route('/hello')
def hello():
    return "Hello World! from " + str(platform.node())

run(host="$ipaddr", port=8080, debug=True)
EOF

sudo chmod 777 /tmp/bottle-app/app.py

python /tmp/bottle-app/app.py &> /var/log/app.log &
